import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import Modal from './modal';
import Child from './child';

const appRoot = document.getElementById('app-root');

class Parent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { clicks: 0 };
  }

  handleClick =() => {
    this.setState(prevState => ({
      clicks: prevState.clicks + 1
    }));
  }

  render() {
    return (
      <div onClick={this.handleClick}>
        <p>Number of clicks: {this.state.clicks}</p>
        <p>
          Open up the browser DevTools
          to observe that the button
          is not a child of the div
          with the onClick handler.
        </p>
        <Modal>
          <Child />
        </Modal>
      </div>
    );
  }
}

ReactDOM.render(<Parent />, appRoot);

export default Parent;
